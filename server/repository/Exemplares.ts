import { default as Model } from '../model/Exemplares'
import { Sequelize, Op as Operetor, DataTypes } from 'sequelize'
import { Json } from 'sequelize/types/lib/utils';
import DataBase from '../database/index';

const BancoDados = new DataBase() 

class ExemplaresRepository {

  constructor() {

  }

  public async BuscarTodos() {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.exemplares(BancoDados.Conexao, Sequelize).findAll({
          where: {}
        }).then((res: any) => {
          resolve(res)
        }).catch((es: any) => {
          reject(es)
        })

      } catch (e) {
        reject(e)
      }
    })
  }

  public async BuscarPorID(id) {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.exemplares(BancoDados.Conexao, Sequelize).findAll({
          where: { id: id}
        }).then((res: any) => {
          resolve(res)
        }).catch((es: any) => {
          reject(es)
        })

      } catch (e) {
        reject(e)
      }
    })
  }

  public async BuscarPorIDPublic(id) {

    return new Promise<Array<Object>>(async (resolve, reject) => {
      try {

        await Model.exemplares(BancoDados.Conexao, Sequelize).findAll({
          where: { idpublic: id}
        }).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject(es)
        })

      } catch (e) {
        reject(e)
      }
    })
  }

  public async Cadastrar(Avaliacao: any) {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.exemplares(BancoDados.Conexao, Sequelize).create(Avaliacao).then((res: any) => {
          resolve(res)
        }).catch((es: any) => {
          reject({
            statusCode: 400,
            titulo: 'Erro ao cadastrar a exemplares [RPSTY]',
            msg: es
          })
        })

      } catch (e) {
        reject({
            statusCode: 400,
            titulo: 'Erro ao cadastrar a exemplares [RPSTY]',
            msg: e
          })
      }
    })
  }

  public async Excluir(id: Number) {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.exemplares(BancoDados.Conexao, Sequelize).destroy({ where: { id: id } }).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject({
            statusCode: 400,
            titulo: 'Erro ao excluir  exemplares [RPSTY]',
            msg: es
          })
        })

      } catch (e) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao excluir exemplares [RPSTY-CATCH]',
          msg: e
        })
      }
    })
  }

  
  public async Atualizar(publicacao: any) {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.exemplares(BancoDados.Conexao, Sequelize).update(publicacao, { where: { id: publicacao.id }}).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject({
            statusCode: 400,
            titulo: 'Erro ao atualizar o exemplar [RPSTY]',
            msg: es
          })
        })

      } catch (e) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao atualizar o exemplar [RPSTY-CATCH]',
          msg: e
        })
      }
    })
  }

}

export default ExemplaresRepository