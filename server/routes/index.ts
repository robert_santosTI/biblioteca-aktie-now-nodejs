import  express from 'express'
import  cors from 'express-cors'
import {PublicacaoRouter} from './Publicacao'
import { AvaliacaoPublicRouter } from "./AvaliacaoPublic";
import { ExemplaresRouter } from "./Exemplares";
import { UsuarioRouter } from "./Usuario";
import { AlocacaoRouter } from "./Alocacao";

const router = express.Router();

// ROTA PARA MONITORAMENTO
router.get('/api-status', (req, res) =>
  res.json({
    status: "ok",
    versao: "1.0.0"
  })
);
router.use(cors({
  allowedOrigins: [
      '*'
  ]
}));

router.use('/publicacao', PublicacaoRouter);
router.use('/avaliacao/publicacao', AvaliacaoPublicRouter);
router.use('/exemplares', ExemplaresRouter);
router.use('/usuario', UsuarioRouter);
router.use('/alocacao', AlocacaoRouter);

export default router ;