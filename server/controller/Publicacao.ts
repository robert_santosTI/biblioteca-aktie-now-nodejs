
import PublicacaoService from '../service/Publicacao'
import { Response, Request } from 'express'
import { Isnull } from '../global/index'

class PublicacaoController_ {
  constructor() {
  }

  static async BuscarTodos(req: Request, res: Response) {
    try {
      let Service = new PublicacaoService
      return await Service
        .ServiceBuscar()
        .then(q => {
          return res.status(200).send(q);
        }).catch(err => {
          res.status(err.statusCode || 500).send(err);
        })
    } catch (e) {
      res.status(e.statusCode || 500).send(e.message);
    }
  }

  static async BuscarPorID(req: Request, res: Response) {
    try {
      let Service = new PublicacaoService
      return await Service
        .ServiceBuscarPorID(Number(req.params.id))
        .then(q => {
          return res.status(200).send(q);
        }).catch(err => {
          res.status(err.statusCode || 500).send(err);
        })
    } catch (e) {
      res.status(e.statusCode || 500).send(e.message);
    }
  }

  static async Criar(req: Request, res: Response) {
     console.log(req.body)
    //DADOS OBRIGATORIO
    if (
      Isnull(req.body.titulo) ||
      Isnull(req.body.descricao) ||
      Isnull(req.body.disponivelpesquisa) ||
      (Number(req.body.disponivelpesquisa) !== 0 && Number(req.body.disponivelpesquisa) !== 1)
    ) {
      res.status(500).send({
        statusCode: 400,
        titulo: 'Preenchar os dados corretamente [CTLL]',
        msg: ''
      })
    } else {
      try {
        let Service = new PublicacaoService
        return await Service
          .ServiceCriar(req.body)
          .then(q => {
            return res.status(200).send(q);
          }).catch(err => {
            res.status(err.statusCode || 500).send(err);
          })
      } catch (e) {
        res.status(e.statusCode || 500).send(e.message);
      }
    }

  }

  static async Atualizar(req: Request, res: Response) {
    try {
      let Service = new PublicacaoService
      return await Service
        .ServiceAtualizar(req.body)
        .then(q => {
          return res.status(200).send(q);
        }).catch(err => {
          res.status(err.statusCode || 500).send(err);
        })
    } catch (e) {
      res.status(e.statusCode || 500).send(e.message);
    }
  }

  static async Excluir(req: Request, res: Response) {
    if (typeof (req.params.id) === 'number') {
      res.status(500).send({
        statusCode: 400,
        titulo: 'O ID da publicação é necessario [CTLL]',
        msg: ''
      })
    } else {
      try {
        let Service = new PublicacaoService
        return await Service
          .ServiceExcluir(Number(req.params.id))
          .then(q => {
            return res.status(200).send();
          }).catch(err => {
            console.log(err)
            res.status(501).send(err);
          })
      } catch (e) {
        console.log(e)
        res.status(501).send(e.message);
      }
    }
  }

}

export var PublicacaoController = PublicacaoController_