class UsuarioModel {
  constructor() {

  }

  static Usuario(queryInterface: any, Sequelize: any) {
    const query = queryInterface.define(
      'usuario',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        nome: {
          type: Sequelize.STRING(80)
        },
        email: {
          type: Sequelize.STRING(80)
        },
        senha: {
          type: Sequelize.STRING(400)
        }
      }, {
      freezeTableName: true,
      timestamps: false
    }
    )
    return query
  }
}

export default UsuarioModel