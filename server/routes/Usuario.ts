import  express from 'express'
import  {UsuarioController} from '../controller/Usuario'

import {verifyJWT} from '../middleware/index'
const Rota = express.Router();

// BUSCAR TODOS
Rota.use(verifyJWT).route('/').get(UsuarioController.BuscarTodos);
// AUTENTICAÇÃO
Rota.route('/autenticacao').post(UsuarioController.Autenticacao);
// CRIAÇÃO
Rota.route('/').post(UsuarioController.Criar);

export  var UsuarioRouter = Rota