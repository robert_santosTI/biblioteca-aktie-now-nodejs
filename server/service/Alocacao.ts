import { Json } from 'sequelize/types/lib/utils'
import { default as Alocacao_ } from '../repository/Alocacao'
import { default as Exemplares_ } from '../repository/Exemplares'
let Alocacao = new Alocacao_()
let Exemplares = new Exemplares_()


class AlocacaoService {
  constructor() {
  }
  public async ServiceBuscarPorID(id: Number) {
    return new Promise(async (resolve, reject) => {
      if (false) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao busca as publicações [SVC]',
          msg: ''
        })
      }
      await Alocacao.BuscarPorID(id).then(async (res: any) => {
        resolve(res)
      }).catch((e) => {
        reject(e)
      })
    })

  }

  public async ServiceBuscar() {
    return new Promise(async (resolve, reject) => {
      if (false) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao busca as publicações [SVC]',
          msg: ''
        })
      }
      await Alocacao.BuscarTodos().then(async (res: any) => {
        resolve(res)
      }).catch((e) => {
        reject(e)
      })
    })

  }

  public async ServiceCriar(alocacao: any) {
    return new Promise(async (resolve, reject) => {
      /*
        Verificar se o exemplar esta disponivel para alocação 
        exemplar status 0 = não alocado || 1 =alocado
      */
      const exemplar: any = await Exemplares.BuscarPorID(alocacao.idexemplar)
      if (exemplar.length == 0)
        return reject({
          statusCode: 400,
          titulo: 'Exemplar para alocação não encontrado',
          msg: ''
        })
      else if (exemplar[0].status !== 0)
        return reject({
          statusCode: 400,
          titulo: 'O exemplar ja encontra-se alocado para um usuário',
          msg: ''
        })
      else
        /*
          Alterando o status do exemplar para 1 alocado
        */
        await Exemplares.Atualizar({
          id: alocacao.idexemplar,
          status: 1
        })
        await Alocacao.Cadastra(alocacao).then(async (res: any) => {
          resolve(res)
        }).catch((e) => {
          reject(e)
        })
    })
  }

  public async ServiceAtualizar(alocacao: Json) {
    return new Promise(async (resolve, reject) => {
      await Alocacao.Atualizar(alocacao).then(async (res: any) => {
        resolve(res)
      }).catch((e) => {
        reject(e)
      })
    })

  }

  public async ServiceExcluir(id: Number) {
    return new Promise(async (resolve, reject) => {

      let AlocacaoD: Array<any> = await Alocacao.BuscarPorID(id)

      if (AlocacaoD.length === 0) 
        return reject({
          statusCode: 400,
          titulo: 'Alocação não encontrada [SVC]',
          msg: 'Essa alocação não existe mais nesse banco de dados '
        })
      else 
        await Alocacao.Excluir(id).then(async (res: any) => {
          resolve(res)
        }).catch((e) => {
          reject(e)
        })
      
    })

  }
}

export default AlocacaoService