import { default as Model } from '../model/AvalicaoPublic'
import { Sequelize, Op as Operetor, DataTypes } from 'sequelize'
import { Json } from 'sequelize/types/lib/utils';
import DataBase from '../database/index';

const BancoDados = new DataBase() 

class AvaliacaoPublicRepository {

  constructor() {

  }

  public async BuscarTodos() {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.AvaliacaoPublic(BancoDados.Conexao, Sequelize).findAll({
          where: {}
        }).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject(es)
        })

      } catch (e) {
        reject(e)
      }
    })
  }

  public async BuscarPorID(id) {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.AvaliacaoPublic(BancoDados.Conexao, Sequelize).findAll({
          where: { id: id}
        }).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject(es)
        })

      } catch (e) {
        reject(e)
      }
    })
  }

  public async BuscarPorIDPublic(id) {

    return new Promise<Array<Object>>(async (resolve, reject) => {
      try {

        await Model.AvaliacaoPublic(BancoDados.Conexao, Sequelize).findAll({
          where: { idpublic: id}
        }).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject(es)
        })

      } catch (e) {
        reject(e)
      }
    })
  }

  public async Cadastrar(Avaliacao: Json) {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.AvaliacaoPublic(BancoDados.Conexao, Sequelize).create(Avaliacao).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject({
            statusCode: 400,
            titulo: 'Erro ao cadastrar a avaliação [RPSTY]',
            msg: es
          })
        })

      } catch (e) {
        reject({
            statusCode: 400,
            titulo: 'Erro ao cadastrar a avaliação [RPSTY]',
            msg: e
          })
      }
    })
  }
}

export default AvaliacaoPublicRepository