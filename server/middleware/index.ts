import  jwt from 'jsonwebtoken';
import { default as config } from '../config/env/index'
export function verifyJWT(req, res, next){
    const token = req.headers.authorization;
    if (!token) return res.status(400).json({
        statusCode: 401,
        titulo: 'Falha de autenticação',
        msg: ''
      });
    
    jwt.verify(token, config.SECRET, function(err, decoded) {
      if (err) return res.status(500).json({
        statusCode: 500,
        titulo: 'Falha de autenticação',
        msg: ''
      });
      
      req.userId = decoded.id;
      next();
    });
}