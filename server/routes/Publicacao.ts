import  express from 'express'

import  {PublicacaoController} from '../controller/Publicacao'
import {verifyJWT} from '../middleware/index'
const Rota = express.Router();


Rota.use(verifyJWT).route('/').get(PublicacaoController.BuscarTodos);
Rota.use(verifyJWT).route('/:id').get(PublicacaoController.BuscarPorID);
Rota.use(verifyJWT).route('/').post(PublicacaoController.Criar);
Rota.use(verifyJWT).route('/:id').delete(PublicacaoController.Excluir);
Rota.use(verifyJWT).route('/').put(PublicacaoController.Atualizar);

export  var PublicacaoRouter = Rota