export var Isnull = (value) => {
    if (value === '' || value === null || value === undefined) {
        return true
    } else {
        return false
    }
}
export var IsNumber = (value) => {
    return typeof (value) === 'number' && !isNaN(value)
}