import ModelAvaliacaoPublic from './AvalicaoPublic'
import ModelExemplares from './Exemplares'

class PublicacaoModel {
  constructor() {

  }

  static Publicacao(queryInterface: any, Sequelize: any) {
    const query = queryInterface.define(
      'publicacao',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        titulo: {
          type: Sequelize.STRING(200)
        },
        descricao: {
          type: Sequelize.STRING(4000)
        },
        disponivelpesquisa: {
          type: Sequelize.INTEGER
        }
      }, {
      freezeTableName: true,
      timestamps: false
    }
    )
    query.hasMany(ModelExemplares.exemplares(queryInterface,Sequelize),{ as: 'exemplares',foreignKey: 'idpublic'})
    ModelExemplares.exemplares(queryInterface,Sequelize).belongsTo(query);

    query.hasMany(ModelAvaliacaoPublic.AvaliacaoPublic(queryInterface,Sequelize),{ as: 'avaliacaopublic',foreignKey: 'idpublic'})
    ModelAvaliacaoPublic.AvaliacaoPublic(queryInterface,Sequelize).belongsTo(query);
    return query
  }
}

export default PublicacaoModel