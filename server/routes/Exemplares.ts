import  express from 'express'
import  {ExemplaresController} from '../controller/Exemplares'

import {verifyJWT} from '../middleware/index'
const Rota = express.Router();

// BUSCAR TODOS
Rota.use(verifyJWT).route('/').get(ExemplaresController.BuscarTodos);
// CRIACAO
Rota.use(verifyJWT).route('/').put(ExemplaresController.Cadastrar);
// EXCLUSÃO
Rota.use(verifyJWT).route('/:id').delete(ExemplaresController.Excluir);

export  var ExemplaresRouter = Rota