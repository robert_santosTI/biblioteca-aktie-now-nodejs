import { default as Model } from '../model/Usuario'
import { Sequelize, Op as Operetor, DataTypes } from 'sequelize'
import { Json } from 'sequelize/types/lib/utils';
import DataBase from '../database/index';

const BancoDados = new DataBase()

class UsuarioRepository {

  constructor() {

  }

  public async BuscarTodos() {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.Usuario(BancoDados.Conexao, Sequelize).findAll({
          where: {},
          attributes: ['id', 'nome']
        }).then((res: any) => {
          resolve(res)
        }).catch((es: any) => {
          reject(es)
        })

      } catch (e) {
        reject(e)
      }
    })
  }

  public async Criar(usuario: any) {

    return new Promise(async (resolve, reject) => {
      try {
        usuario.senha =
          await Model.Usuario(BancoDados.Conexao, Sequelize).create(usuario).then((res: any) => {
            resolve(res)
          }).catch((es: Json) => {
            reject({
              statusCode: 400,
              titulo: 'Erro ao cadastrar o usuario [RPSTY]',
              msg: es
            })
          })

      } catch (e) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao cadastrar o usuario [RPSTY-CATCH]',
          msg: e
        })
      }
    })
  }


  public async BuscarPorEmail(email) {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.Usuario(BancoDados.Conexao, Sequelize).findAll({
          where: {email: email}
        }).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject(es)
        })

      } catch (e) {
        reject(e)
      }
    })
  }
}

export default UsuarioRepository