import { Json } from 'sequelize/types/lib/utils'
import { default as AvaliacaoPublic_ } from '../repository/AvaliacaoPublic'
let AvaliacaoPublic = new AvaliacaoPublic_()

class AvaliacaoPublicService {
  constructor() {
  }

  public async ServiceBuscarPorID (id: Number) {

    return new Promise (async (resolve, reject) => {
      if (false) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao busca as publicações [SVC]',
          msg: ''
        })
      }
      await AvaliacaoPublic.BuscarPorID(id).then(async (res: any)=>{
        resolve(res)
      }).catch((e) => {
        reject (e)
      })
    })

  }

  public async ServiceBuscar () {

    return new Promise (async (resolve, reject) => {
      if (false) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao busca as publicações [SVC]',
          msg: ''
        })
      }
      await AvaliacaoPublic.BuscarTodos().then(async (res: any)=>{
        resolve(res)
      }).catch((e) => {
        reject (e)
      })
    })

  }

  public async ServiceCadastrar(Avaliacao: any) {
    return new Promise(async (resolve, reject) => {
      if (false) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao cadastrar a avaliação [SVC]',
          msg: ''
        })
      }
      await AvaliacaoPublic.Cadastrar(Avaliacao).then(async (res: any) => {
        resolve(res)
      }).catch((e) => {
        reject(e)
      })
    })
  }
}
export default AvaliacaoPublicService