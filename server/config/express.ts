import  express from 'express'
import  cors from 'cors'
import {default as bodyParser}  from 'body-parser'
import {default as config} from './env/index'
import {default as router} from '../routes/index'
//import consign  from 'consign'

class app {
    public  express : express.Application
    
    constructor () {
        this.express = express()
        this.server()
    }

    private server() {  
        this.express.listen(config.portApi)    
        this.express.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization, tokennow");
            res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
            res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
            next();
        })
        console.log(`Servidor rodando em http://localhost:${config.portApi}`)
        this.express.use(bodyParser.json())
        this.express.use(cors())
        this.express.use(bodyParser.urlencoded({ extended: true }))
        this.express.use('/api/', router)
    }
}

export default app
