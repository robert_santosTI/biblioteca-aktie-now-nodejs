import AvalicaoPublicService from '../service/AvalicaoPublic'
import { Response, Request } from 'express'

class AvalicaoPublicController_ {
    constructor() {
    }

    static async BuscarPorID(req: Request, res: Response) {
      try {
        let Service = new AvalicaoPublicService
        return await Service
          .ServiceBuscarPorID(Number(req.params.id))
          .then(q => {
            return res.status(200).send(q);
          }).catch(err => {
            res.status(err.statusCode || 500).send(err);
          })
      } catch (e) {
        res.status(e.statusCode || 500).send(e.message);
      }
    }

    static async BuscarTodos(req: Request, res: Response) {
      try {
        let Service = new AvalicaoPublicService
        return await Service
          .ServiceBuscar()
          .then(q => {
            return res.status(200).send(q);
          }).catch(err => {
            res.status(err.statusCode || 500).send(err);
          })
      } catch (e) {
        res.status(e.statusCode || 500).send(e.message);
      }
    }

    static async Cadastrar(req: Request, res: Response) {
        try {
            let Service = new AvalicaoPublicService
            return await Service
                .ServiceCadastrar(req.body)
                .then(q => {
                    return res.status(200).send(q);
                }).catch(err => {
                    res.status(err.statusCode || 500).send(err);
                })
        } catch (e) {
            res.status(e.statusCode || 500).send(e.message);
        }
    }

}

export var AvaliacaoPublicController = AvalicaoPublicController_