class AlocacaoModel {
    constructor() {

    }
    static alocacao(queryInterface: any, Sequelize: any) {
        const query = queryInterface.define(
            'alocacao',
            {
                id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                idpublic: {
                    type: Sequelize.INTEGER,
                    references: {
                      model: 'publicacao',
                      key: 'id'
                    }
                },
                idexemplar: {
                    type: Sequelize.INTEGER,
                    references: {
                      model: 'exemplares',
                      key: 'id'
                    }
                },
                idusuario: {
                    type: Sequelize.INTEGER
                },
                status: {
                    type: Sequelize.INTEGER
                },
                datainicial: {
                    type: Sequelize.DATE
                },
                datafinal: {
                    type: Sequelize.DATE
                }
            }, {
            freezeTableName: true,
            timestamps: false
        }
        )
        return query
    }
}

export default AlocacaoModel