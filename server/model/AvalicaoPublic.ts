import ModelPublic from './Publicacao'
class AvaliacaoPublicModel {
    constructor() {

    }

    static AvaliacaoPublic(queryInterface: any, Sequelize: any) {
        const query = queryInterface.define(
            'avaliacaopublic',
            {
                id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                idpublic: {
                    type: Sequelize.INTEGER,
                    references: {
                      model: 'publicacao',
                      key: 'id'
                    }
                },
                idusuario: {
                    type: Sequelize.INTEGER
                },
                nota: {
                    type: Sequelize.INTEGER
                },
                dtavaliacao: {
                    type: Sequelize.DATE,
                    defaultValue: Sequelize.fn('NOW')
                }
            }, {
            freezeTableName: true,
            timestamps: false
        }
        )
        return query
    }
}

export default AvaliacaoPublicModel