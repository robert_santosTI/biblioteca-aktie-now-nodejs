import  express from 'express'
import  {AvaliacaoPublicController} from '../controller/AvaliacaoPublic'

import {verifyJWT} from '../middleware/index'
const Rota = express.Router();

// BUSCAR TODOS
Rota.use(verifyJWT).route('/').get(AvaliacaoPublicController.BuscarTodos);
// CRIACAO
Rota.use(verifyJWT).route('/').put(AvaliacaoPublicController.Cadastrar);

export  var AvaliacaoPublicRouter = Rota