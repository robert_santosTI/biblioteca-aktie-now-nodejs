import { default as Model } from '../model/Publicacao'
import { Sequelize, Op as Operetor, DataTypes } from 'sequelize'
import { Json } from 'sequelize/types/lib/utils';
import DataBase from '../database/index';

const BancoDados = new DataBase()

class PublicacaoRepository {

  constructor() {

  }

  public async BuscarTodos() {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.Publicacao(BancoDados.Conexao, Sequelize).findAll({
          where: {},
          include: [
            {
              association: 'avaliacaopublic',
              required: false
            },
            {
              association: 'exemplares',
              required: false
            }],
          attributes: [
            'id',
            'titulo',
            'descricao',
            'disponivelpesquisa',
            [BancoDados.Conexao.literal('(SELECT count(1) FROM `exemplares` WHERE `exemplares`.`idpublic` = `publicacao`.`id` and `exemplares`.`status` = 0)'), 'exemplaresdisponivel'],
            [BancoDados.Conexao.literal('(SELECT avg(`avaliacaopublic`.`nota`) FROM `avaliacaopublic` WHERE `avaliacaopublic`.`idpublic` = `publicacao`.`id`)'), 'media']
          ],
          order: [[`id`, 'desc']]
        }).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject(es)
        })

      } catch (e) {
        reject(e)
      }
    })
  }

  public async BuscarPorID(id: Number) {

    return new Promise<Array<Object>>(async (resolve, reject) => {
      try {

        await Model.Publicacao(BancoDados.Conexao, Sequelize).findAll({
          where: { id },
          include: [
            {
              association: 'avaliacaopublic',
              required: false
            },
            {
              association: 'exemplares',
              required: false
            }],
          attributes: [
            'id',
            'titulo',
            'descricao',
            'disponivelpesquisa',
            [BancoDados.Conexao.literal('(SELECT count(1) FROM `exemplares` WHERE `exemplares`.`idpublic` = `publicacao`.`id` and `exemplares`.`status` = 0)'), 'exemplaresdisponivel'],
            [BancoDados.Conexao.literal('(SELECT avg(`avaliacaopublic`.`nota`) FROM `avaliacaopublic` WHERE `avaliacaopublic`.`idpublic` = `publicacao`.`id`)'), 'media']
          ]
        }).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject(es)
        })

      } catch (e) {
        reject(e)
      }
    })
  }

  public async Criar(publicacao: Json) {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.Publicacao(BancoDados.Conexao, Sequelize).create(publicacao).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject({
            statusCode: 400,
            titulo: 'Erro ao cadastrar a Publicacao [RPSTY]',
            msg: es
          })
        })

      } catch (e) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao cadastrar a Publicacao [RPSTY-CATCH]',
          msg: e
        })
      }
    })
  }

  public async Atualizar(publicacao: any) {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.Publicacao(BancoDados.Conexao, Sequelize).update(publicacao, { where: { id: publicacao.id }}).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject({
            statusCode: 400,
            titulo: 'Erro ao cadastrar a Publicacao [RPSTY]',
            msg: es
          })
        })

      } catch (e) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao cadastrar a Publicacao [RPSTY-CATCH]',
          msg: e
        })
      }
    })
  }

  public async Excluir(id: Number) {

    return new Promise(async (resolve, reject) => {
      try {

        await Model.Publicacao(BancoDados.Conexao, Sequelize).destroy({ where: { id: id } }).then((res: any) => {
          resolve(res)
        }).catch((es: Json) => {
          reject({
            statusCode: 400,
            titulo: 'Erro ao cadastrar a publicacao [RPSTY]',
            msg: es
          })
        })

      } catch (e) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao cadastrar a publicacao [RPSTY-CATCH]',
          msg: e
        })
      }
    })
  }
}

export default PublicacaoRepository