import ExemplaresService from '../service/Exemplares'
import { Response, Request } from 'express'
import { Isnull } from '../global/index'

class ExemplaresController_ {

  constructor() {
  }

  static async BuscarPorID(req: Request, res: Response) {
    try {
      let Service = new ExemplaresService
      return await Service
        .ServiceBuscarPorID(Number(req.params.id))
        .then(q => {
          return res.status(200).send(q);
        }).catch(err => {
          res.status(err.statusCode || 500).send(err);
        })
    } catch (e) {
      res.status(e.statusCode || 500).send(e.message);
    }
  }

  static async BuscarTodos(req: Request, res: Response) {
    try {
      let Service = new ExemplaresService
      return await Service
        .ServiceBuscar()
        .then(q => {
          return res.status(200).send(q);
        }).catch(err => {
          res.status(err.statusCode || 500).send(err);
        })
    } catch (e) {
      res.status(e.statusCode || 500).send(e.message);
    }
  }

  static async Cadastrar(req: Request, res: Response) {
    console.log(req.body)
    if (Isnull(req.body.ano) || Isnull(req.body.edicao) || Isnull(req.body.tombo)) {
      res.status(500).send({
        statusCode: 400,
        titulo: 'Preenchar os dados corretamente [CTLL]',
        msg: ''
      })
    } else {
      try {
        let Service = new ExemplaresService
        return await Service
          .ServiceCadastrar(req.body)
          .then(q => {
            return res.status(200).send(q);
          }).catch(err => {
            res.status(err.statusCode || 500).send(err);
          })
      } catch (e) {
        res.status(e.statusCode || 500).send(e.message);
      }
    }
  }


  static async Excluir(req: Request, res: Response) {
    if (typeof (req.params.id) === 'number') {
      res.status(500).send({
        statusCode: 400,
        titulo: 'O ID do exemplar é necessario [CTLL]',
        msg: ''
      })
    } else {
      try {
        let Service = new ExemplaresService
        return await Service
          .ServiceExcluir(Number(req.params.id))
          .then(q => {
            return res.status(200).send();
          }).catch(err => {
            console.log(err)
            res.status(501).send(err);
          })
      } catch (e) {
        console.log(e)
        res.status(501).send(e.message);
      }
    }
  }

}

export var ExemplaresController = ExemplaresController_