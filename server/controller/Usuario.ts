import UsuarioService from '../service/Usuario'
import { Response, Request } from 'express'

class UsuarioController_ {

  constructor() {
  }

  static async BuscarTodos(req: Request, res: Response) {
    try {
      let Service = new UsuarioService
      return await Service
        .ServiceBuscar()
        .then(q => {
          return res.status(200).send(q);
        }).catch(err => {
          res.status(err.statusCode || 500).send(err);
        })
    } catch (e) {
      res.status(e.statusCode || 500).send(e.message);
    }
  }
  
  static async Autenticacao(req: Request, res: Response) {
    try {
      let Service = new UsuarioService
      return await Service
        .ServiceAutetincacao(req.body)
        .then(q => {
          return res.status(200).send(q);
        }).catch(err => {
          res.status(err.statusCode || 500).send(err);
        })
    } catch (e) {
      res.status(e.statusCode || 500).send(e.message);
    }
  }
  static async Criar(req: Request, res: Response) {
    try {
      let Service = new UsuarioService
      return await Service
        .ServiceCriar(req.body)
        .then(q => {
          return res.status(200).send(q);
        }).catch(err => {
          res.status(err.statusCode || 500).send(err);
        })
    } catch (e) {
      res.status(e.statusCode || 500).send(e.message);
    }
  }
  

}

export var UsuarioController = UsuarioController_