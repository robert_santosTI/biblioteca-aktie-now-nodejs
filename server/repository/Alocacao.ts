import { default as Model } from '../model/Alocacao'
import { Sequelize, Op as Operetor, DataTypes } from 'sequelize'
import { Json } from 'sequelize/types/lib/utils';
import DataBase from '../database/index';

const BancoDados = new DataBase()

class AlocacaoRepository {

    constructor() {

    }

    public async BuscarTodos() {

        return new Promise<Array<Object>>(async (resolve, reject) => {
            try {

                await Model.alocacao(BancoDados.Conexao, Sequelize).findAll({
                    where: {}
                }).then((res: any) => {
                    resolve(res)
                }).catch((es: Json) => {
                    reject(es)
                })

            } catch (e) {
                reject(e)
            }
        })
    }

    public async BuscarPorID(id) {

        return new Promise<Array<Object>>(async (resolve, reject) => {
            try {

                await Model.alocacao(BancoDados.Conexao, Sequelize).findAll({
                    where: { id: id }
                }).then((res: any) => {
                    resolve(res)
                }).catch((es: Json) => {
                    reject(es)
                })

            } catch (e) {
                reject(e)
            }
        })
    }

    public async BuscarPorIDPublic(id) {

        return new Promise<Array<Object>>(async (resolve, reject) => {
            try {

                await Model.alocacao(BancoDados.Conexao, Sequelize).findAll({
                    where: { idpublic: id }
                }).then((res: any) => {
                    resolve(res)
                }).catch((es: Json) => {
                    reject(es)
                })

            } catch (e) {
                reject(e)
            }
        })
    }

    public async Cadastra(Alocacao: Json) {

        return new Promise(async (resolve, reject) => {
            try {

                await Model.alocacao(BancoDados.Conexao, Sequelize).create(Alocacao).then((res: any) => {
                    resolve(res)
                }).catch((es: Json) => {
                    reject({
                        statusCode: 400,
                        titulo: 'Erro ao cadastrar a alocação [RPSTY]',
                        msg: es
                    })
                })

            } catch (e) {
                reject({
                    statusCode: 400,
                    titulo: 'Erro ao cadastrar a alocação [RPSTY]',
                    msg: e
                })
            }
        })
    }

    public async Atualizar(publicacao: any) {

        return new Promise(async (resolve, reject) => {
            try {

                await Model.alocacao(BancoDados.Conexao, Sequelize).update(publicacao, { where: { id: publicacao.id } }).then((res: any) => {
                    resolve(res)
                }).catch((es: Json) => {
                    reject({
                        statusCode: 400,
                        titulo: 'Erro ao atualizar alocação [RPSTY]',
                        msg: es
                    })
                })

            } catch (e) {
                reject({
                    statusCode: 400,
                    titulo: 'Erro ao atualizar alocação [RPSTY-CATCH]',
                    msg: e
                })
            }
        })
    }

    public async Excluir(id: Number) {

        return new Promise(async (resolve, reject) => {
            try {

                await Model.alocacao(BancoDados.Conexao, Sequelize).destroy({ where: { id: id } }).then((res: any) => {
                    resolve(res)
                }).catch((es: Json) => {
                    reject({
                        statusCode: 400,
                        titulo: 'Erro ao deleta alocação [RPSTY]',
                        msg: es
                    })
                })

            } catch (e) {
                reject({
                    statusCode: 400,
                    titulo: 'Erro ao deleta alocação [RPSTY-CATCH]',
                    msg: e
                })
            }
        })
    }
}

export default AlocacaoRepository