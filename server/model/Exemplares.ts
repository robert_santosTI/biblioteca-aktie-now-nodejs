class ExemplaresModel {
    constructor() {
  
    }
  
    static exemplares(queryInterface: any, Sequelize: any) {
      const query = queryInterface.define(
        'exemplares',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },
          idpublic: {
            type: Sequelize.INTEGER,
            requerid: true,
            references: {
              model: 'publicacao',
              key: 'id'
            }
          },
          tombo: {
            type: Sequelize.INTEGER
          },
          ano: {
            type: Sequelize.INTEGER
          },
          edicao: {
            type: Sequelize.STRING(30)
          },
          status: {
            type: Sequelize.INTEGER
          }
        }, {
        freezeTableName: true,
        timestamps: false
      }
      )
      return query
    }
  }
  
  export default ExemplaresModel