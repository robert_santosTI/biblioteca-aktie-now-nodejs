
import AlocacaoService from '../service/Alocacao'
import { Response, Request } from 'express'
import { Isnull, IsNumber } from '../global/index'

class AlocacaoController_ {
    constructor() {
    }

    static async BuscarTodos(req: Request, res: Response) {
        try {
            let Service = new AlocacaoService
            return await Service
                .ServiceBuscar()
                .then(q => {
                    return res.status(200).send(q);
                }).catch(err => {
                    res.status(err.statusCode || 500).send(err);
                })
        } catch (e) {
            res.status(e.statusCode || 500).send(e.message);
        }
    }

    static async BuscarPorID(req: Request, res: Response) {
        try {
            if (Isnull(req.params.id))
                return res.status(500).send({
                    titulo: 'Preenchar os dados corretamente [CTLL]',
                    msg: ''
                })

            let Service = new AlocacaoService
            return await Service
                .ServiceBuscarPorID(Number(req.params.id))
                .then(q => {
                    return res.status(200).send(q);
                }).catch(err => {
                    res.status(err.statusCode || 500).send(err);
                })
        } catch (e) {
            res.status(e.statusCode || 500).send(e.message);
        }
    }

    static async Criar(req: Request, res: Response) {
        //DADOS OBRIGATORIO
        if (
            Isnull(req.body.datainicial) ||
            Isnull(req.body.datafinal) ||
            Isnull(req.body.idexemplar) ||
            Isnull(req.body.idpublic) ||
            Isnull(req.body.idusuario)
        )
            return res.status(500).send({
                titulo: 'Preenchar os dados corretamente [CTLL]',
                msg: ''
            })

        try {
            let Service = new AlocacaoService
            return await Service
                .ServiceCriar(req.body)
                .then(q => {
                    return res.status(200).send(q);
                }).catch(err => {
                    res.status(err.statusCode || 500).send(err);
                })
        } catch (e) {
            res.status(e.statusCode || 500).send(e.message);
        }


    }

    static async Atualizar(req: Request, res: Response) {
        try {
            let Service = new AlocacaoService
            return await Service
                .ServiceAtualizar(req.body)
                .then(q => {
                    return res.status(200).send(q);
                }).catch(err => {
                    res.status(err.statusCode || 500).send(err);
                })
        } catch (e) {
            res.status(e.statusCode || 500).send(e.message);
        }
    }

    static async Excluir(req: Request, res: Response) {
        if (IsNumber(Number(req.params.id)))
            return res.status(500).send({
                statusCode: 400,
                titulo: 'O ID da alocação é necessario [CTLL]',
                msg: ''
            })

        try {
            let Service = new AlocacaoService
            return await Service
                .ServiceExcluir(Number(req.params.id))
                .then(q => {
                    return res.status(200).send();
                }).catch(err => {
                    console.log(err)
                    res.status(501).send(err);
                })
        } catch (e) {
            console.log(e)
            res.status(501).send(e.message);
        }


    }
}

export var AlocacaoController = AlocacaoController_