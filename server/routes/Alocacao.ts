import  express from 'express'
import  {AlocacaoController} from '../controller/Alocacao'
import {verifyJWT} from '../middleware/index'
const Rota = express.Router();


Rota.use(verifyJWT).route('/').get(AlocacaoController.BuscarTodos);
Rota.use(verifyJWT).route('/:id').get(AlocacaoController.BuscarPorID);
Rota.use(verifyJWT).route('/').post(AlocacaoController.Criar);
Rota.use(verifyJWT).route('/:id').delete(AlocacaoController.Excluir);
Rota.use(verifyJWT).route('/').put(AlocacaoController.Atualizar);

export  var AlocacaoRouter = Rota