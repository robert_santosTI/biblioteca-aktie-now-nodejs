import { default as config } from '../config/env/index'
import { Sequelize } from 'sequelize'

class DataBase {
    public Info;
    public Conexao;
    constructor() {
       this.Info = config.dataConfig.MYSQL;
        this.Conexao = new Sequelize(this.Info .database, this.Info .user, this.Info .password, {
            host: this.Info.host,
            port: this.Info.port,
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            },
            dialectOptions: {
                //useUTC: true,
            },
            timezone: '-03:00'
        })
        this.Conexao.authenticate().then(() => {
            console.log('Conexão com o banco de dados estabelecida com sucesso');
        }).catch(err => {
            console.error('Erro ao estabelece conexão com o banco de dados: ', err);
        })
    }
}

export default DataBase