import { Json } from 'sequelize/types/lib/utils'
import { default as Exemplares_ } from '../repository/Exemplares'
let Exemplares = new Exemplares_()

class ExemplaresService {
    constructor() {
    }

    public async ServiceBuscarPorID(id: Number) {

        return new Promise(async (resolve, reject) => {
            if (false) {
                reject({
                    statusCode: 400,
                    titulo: 'Erro ao busca as exemplares [SVC]',
                    msg: ''
                })
            }
            await Exemplares.BuscarPorID(id).then(async (res: any) => {
                resolve(res)
            }).catch((e) => {
                reject(e)
            })
        })

    }

    public async ServiceBuscar() {

        return new Promise(async (resolve, reject) => {
            if (false) {
                reject({
                    statusCode: 400,
                    titulo: 'Erro ao busca as exemplares [SVC]',
                    msg: ''
                })
            }
            await Exemplares.BuscarTodos().then(async (res: any) => {
                resolve(res)
            }).catch((e) => {
                reject(e)
            })
        })

    }

    public async ServiceCadastrar(Exemplar: any) {
        return new Promise(async (resolve, reject) => {
            if (false) {
                reject({
                    statusCode: 400,
                    titulo: 'Erro ao cadastrar a exemplares [SVC]',
                    msg: ''
                })
            }
            await Exemplares.Cadastrar(Exemplar).then(async (res: any) => {
                resolve(res)
            }).catch((e) => {
                reject(e)
            })
        })
    }


    public async ServiceExcluir(id: Number) {
        return new Promise(async (resolve, reject) => {

            await Exemplares.Excluir(id).then(async (res: any) => {
                resolve(res)
            }).catch((e) => {
                reject(e)
            })
        })

    }

}
export default ExemplaresService