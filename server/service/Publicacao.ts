import { Json } from 'sequelize/types/lib/utils'
import { default as Publicacao_ } from '../repository/Publicacao'
import { default as AvaliacaoPublic_ } from '../repository/AvaliacaoPublic'
let Publicacao = new Publicacao_()
let AvaliacaoPublic = new AvaliacaoPublic_()


class PublicacaoService {
  constructor() {
  }
  public async ServiceBuscarPorID(id: Number) {
    return new Promise(async (resolve, reject) => {
      if (false) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao busca as publicações [SVC]',
          msg: ''
        })
      }
      await Publicacao.BuscarPorID(id).then(async (res: any) => {
        resolve(res)
      }).catch((e) => {
        reject(e)
      })
    })

  }

  public async ServiceBuscar() {
    return new Promise(async (resolve, reject) => {
      if (false) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao busca as publicações [SVC]',
          msg: ''
        })
      }
      await Publicacao.BuscarTodos().then(async (res: any) => {
        resolve(res)
      }).catch((e) => {
        reject(e)
      })
    })

  }

  public async ServiceCriar(publicacao: any) {
    return new Promise(async (resolve, reject) => {
       
        await Publicacao.Criar(publicacao).then(async (res: any) => {
          resolve(res)
        }).catch((e) => {
          reject(e)
        })
    })
  }

  public async ServiceAtualizar(publicacao: Json) {
    return new Promise(async (resolve, reject) => {
      if (false) {
        reject({
          statusCode: 400,
          titulo: 'Erro ao busca as publicações [SVC]',
          msg: ''
        })
      }
      await Publicacao.Atualizar(publicacao).then(async (res: any) => {
        resolve(res)
      }).catch((e) => {
        reject(e)
      })
    })

  }

  public async ServiceExcluir(id: Number) {
    return new Promise(async (resolve, reject) => {

      let PublicacaoD: Array<any> = await Publicacao.BuscarPorID(id)
      let Avaliacaoes: Array<any> = await AvaliacaoPublic.BuscarPorIDPublic(PublicacaoD[0].id)

      if (PublicacaoD.length === 0) {
        reject({
          statusCode: 400,
          titulo: 'Livro não encontrada [SVC]',
          msg: 'Esse livro não existe mais nesse banco de dados '
        })
      } else if (Avaliacaoes.length > 0) {
        reject({
          statusCode: 400,
          titulo: 'Existe avaliações vinculado ao livro [SVC]',
          msg: 'Por favor excluar as avaliações vinculado ao livro'
        })
      } else {
        await Publicacao.Excluir(id).then(async (res: any) => {
          resolve(res)
        }).catch((e) => {
          reject(e)
        })
      }
    })

  }
}

export default PublicacaoService