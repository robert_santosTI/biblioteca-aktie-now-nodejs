import { Json } from 'sequelize/types/lib/utils'
import { default as Usuario_ } from '../repository/Usuario'
import bcrypt from 'bcrypt';
import { default as config } from '../config/env/index'
import  jwt from 'jsonwebtoken';

let Usuario = new Usuario_()

class UsuarioService {
  constructor() {
  }

  public async ServiceCriar(usuario: any) {
    return new Promise(async (resolve, reject) => {

      bcrypt.hash(usuario.senha, config.saltRounds, async function (err, hash) {
        usuario.senha = hash
        await Usuario.Criar(usuario).then(async (res: any) => {
          resolve(res)
        }).catch((e) => {
          reject(e)
        })
      });

    })
  }

  public async ServiceBuscar() {
    return new Promise(async (resolve, reject) => {

      await Usuario.BuscarTodos().then(async (res: any) => {
        resolve(res)
      }).catch((e) => {
        reject(e)
      })

    })
  }

  public async ServiceAutetincacao(user) {
    return new Promise(async (resolve, reject) => {
      let usuario: any = await Usuario.BuscarPorEmail(user.email)
      if (usuario.length == 0)
        return reject({
          statusCode: 400,
          titulo: 'Usuario ou senha incorreta!!!',
          msg: ''
        })
      else
        bcrypt.compare(user.senha, usuario[0].senha, (err, result) => {
          if (result) {
            const token = jwt.sign({ id: usuario[0].id }, config.SECRET, {
              expiresIn: 2400
            });
            return resolve({ auth: true, token: token });
          } else {
            return reject({
              statusCode: 400,
              titulo: 'Usuario ou senha incorreta!!',
              msg: err
            });
          }
        });


    })
  }


}

export default UsuarioService